﻿using FAF.BT.DataAccess;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace Daltest
{
    
    public static class Helper
    {
        static string DataCleanUp = "DataCleanUp";
        public static DataTable ExecuteDBProcedure(string scriptName, params object[] parms)
        {
            object[] Responseparams = new object[10];
            DataSet dsGABresult = DAL.ExecuteStoredProcWithOutput(scriptName, out Responseparams, parms);
            if (dsGABresult != null && dsGABresult.Tables.Count > 0 && dsGABresult.Tables[0].Rows.Count > 0)
            {
                return dsGABresult.Tables[0];
            }
            else return null;
        }
        public static double GetTimeFromString(string time = "")
        {
            double returnval = 0;
            if (time != null)
            {
                string hours = string.Empty;
                string mins = string.Empty;
                time = isTruthyValue(time);
                if (!string.IsNullOrEmpty(time))
                {
                    string[] parts = time.Split(':');
                    hours = parts[0];
                    mins = parts[1];
                }
                else
                {
                    hours = DateTime.Now.Hour.ToString();
                    mins = DateTime.Now.Minute.ToString();
                }
                string sdatatime = hours + ".0" + mins.TrimEnd('0');
                returnval = Convert.ToDouble(sdatatime);
            }
            return returnval;
        }
        public static string GetDataCleanUpDB()
        {
            try
            {
                string dbname = isTruthyValue(ConfigurationManager.AppSettings["ConnectionString"]);
                string[] list = dbname.Split(new string[] { "FASTPROD" }, StringSplitOptions.None);
                if (list.Count() > 1)
                {
                    return list[0] + "DataCleanUp" + list[1];
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return string.Empty;

        }
        public static DataSet ExecuteDBProcedureDataset(int SwitchDB, string scriptName, params object[] parms)
        {
            if (SwitchDB == 1)
            {
                object objConnStr = DAL.ConnectionStringSettingForCurrentThread;
                try
                {
                    using (new DynamicDBConnection(DataCleanUp))
                    {
                        object[] Responseparams = new object[10];
                        DataSet dsGABresult = DAL.ExecuteStoredProcWithOutput(scriptName, out Responseparams, parms);
                        if (dsGABresult != null && dsGABresult.Tables.Count > 0 && dsGABresult.Tables[0].Rows.Count > 0)
                        {
                            return dsGABresult;
                        }
                        else return null;
                    }
                }
                finally
                {
                    DAL.ConnectionStringSettingForCurrentThread = objConnStr;
                }
            }
            else
            {
                object[] Responseparams = new object[10];
                DataSet dsGABresult = DAL.ExecuteStoredProcWithOutput(scriptName, out Responseparams, parms);
                if (dsGABresult != null && dsGABresult.Tables.Count > 0 && dsGABresult.Tables[0].Rows.Count > 0)
                {
                    return dsGABresult;
                }
                else return null;
            }
        }
        public static DataTable ExecuteDBProcedure(int SwitchDB, string scriptName, params object[] parms)
        {
            if (SwitchDB == 1)
            {
                object objConnStr = DAL.ConnectionStringSettingForCurrentThread;
                try
                {
                    using (new DynamicDBConnection(DataCleanUp))
                    {
                        object[] Responseparams = new object[10];
                        DataSet dsGABresult = DAL.ExecuteStoredProcWithOutput(scriptName, out Responseparams, parms);
                        if (dsGABresult != null && dsGABresult.Tables.Count > 0 && dsGABresult.Tables[0].Rows.Count > 0)
                        {
                            return dsGABresult.Tables[0];
                        }
                        else return null;
                    }
                }
                finally
                {
                    DAL.ConnectionStringSettingForCurrentThread = objConnStr;
                }
            }
            else
            {
                object[] Responseparams = new object[10];
                DataSet dsGABresult = DAL.ExecuteStoredProcWithOutput(scriptName, out Responseparams, parms);
                if (dsGABresult != null && dsGABresult.Tables.Count > 0 && dsGABresult.Tables[0].Rows.Count > 0)
                {
                    return dsGABresult.Tables[0];
                }
                else return null;
            }
        }
        public static DataSet ExecuteDBSETProcedure(string scriptName, params object[] parms)
        {
            object[] Responseparams = new object[10];
            //DAL.ConnectionStringSettingForCurrentThread
            DataSet dsGABresult = DAL.ExecuteStoredProcWithOutput(scriptName, out Responseparams, parms);
            if (dsGABresult != null && dsGABresult.Tables.Count > 0 && dsGABresult.Tables[0].Rows.Count > 0)
            {
                return dsGABresult;//.Tables[0];
            }
            else return null;
        }

        public static void Execute(int SwitchDB, string scriptName, params object[] parms)
        {
            if (SwitchDB == 1)
            {
                object objConnStr = DAL.ConnectionStringSettingForCurrentThread;
                try
                {
                    using (new DynamicDBConnection(DataCleanUp))
                    {
                        DAL.Execute(scriptName, parms);
                    }
                }
                finally
                {
                    DAL.ConnectionStringSettingForCurrentThread = objConnStr;
                }
            }
            else { DAL.Execute(scriptName, parms); }

        }



        private static string removeInvalidXmlChars(string inputXml)
        {
            char[] validXmlChars = inputXml.Where(ch => XmlConvert.IsXmlChar(ch)).ToArray();
            return new string(validXmlChars);
        }
        public static DateTime isTruthyValueDateTime(object obj)
        {
            DateTime returnVal = new DateTime();
            DateTime date = new DateTime();
            bool d  = DateTime.TryParse(isTruthyValue(obj),out date);
            if(d)
            {
                returnVal = date;
            }
            return returnVal;
        }

        public static string isTruthyValue(object obj)
        {
            string returnVal = string.Empty;
            if (obj != null && !string.IsNullOrEmpty(obj.ToString()))
            {
                string text = Convert.ToString(obj);
                returnVal = removeInvalidXmlChars(text);
                returnVal = returnVal.Trim();
            }
            return returnVal;
        }
        public static HelperReturn GetDataTableFromObj(Dictionary<string, dynamic> obj, int dataTypeObj)
        {
            HelperReturn h = new HelperReturn();
            try
            {
                if (obj != null && obj["Output"] != null)
                {

                    switch (dataTypeObj)
                    {
                        case (int)DataTypeObj.DataSet:
                            DataSet ds = JsonConvert.DeserializeObject<DataSet>(obj["Output"]);
                            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                            {
                                h.HasValue = true;
                                h.Value = ds;
                            }
                            break;
                        case (int)DataTypeObj.DataTable:

                            DataTable dt = JsonConvert.DeserializeObject<DataTable>(obj["Output"]);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                h.HasValue = true;
                                h.Value = dt;
                            }
                            break;
                        case (int)DataTypeObj.None:
                            object val = JsonConvert.DeserializeObject(obj["Output"]);
                            if (val != null)
                            {
                                h.HasValue = true;
                                h.Value = val;
                            }
                            break;
                        case (int)DataTypeObj.SpecialDataSet:
                            DataSet dss = ReadDataFromJson((obj["Output"]));
                            if (dss != null && dss.Tables.Count > 0 && dss.Tables[0].Rows.Count > 0)
                            {
                                h.HasValue = true;
                                h.Value = dss;
                            }
                            break;
                        case (int)DataTypeObj.DataSetRawQuery:
                            DataSet dsss = ReadDataFromJson((obj["Output"]));
                            dsss =  ReadDataFromJson(dsss.Tables[0].Rows[0]["Output"].ToString());
                            if (dsss != null && dsss.Tables.Count > 0 && dsss.Tables[0].Rows.Count > 0)
                            {
                                h.HasValue = true;
                                h.Value = dsss;
                            }
                            break;
                    }

                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return h;
        }
        private static DataSet ReadDataFromJson(String jsonString)
        {
            //// Using XML as an interface of deserializing
            var xd = new XmlDocument();

            //// Note:Json convertor needs a json with one node as root
            jsonString = "{ \"rootNode\": {" + jsonString.Trim().TrimStart('{').TrimEnd('}') + @"} }";
            //// Now it is secure that we have always a Json with one node as root 
            xd = JsonConvert.DeserializeXmlNode(jsonString);

            //// DataSet is able to read from XML and return a proper DataSet
            var result = new DataSet();
            result.ReadXml(new XmlNodeReader(xd));
            return result;
        }
        public static string isTruthyValueConfig(string confValue)
        {
            string returnFlag = string.Empty;
            string value = string.Empty;
            try
            {
                value = isTruthyValue(ConfigurationManager.AppSettings[confValue]);
            }

            catch (ConfigurationErrorsException configErrEx)
            {
                throw configErrEx;
            }

            catch (Exception e)
            {
                throw e;
            }
            if (!string.IsNullOrEmpty(value) && !string.IsNullOrWhiteSpace(value))
            {
                returnFlag = value;
            }
            return returnFlag;
        }

        public static bool isTruthyValuebool(string confValue)
        {
            bool returnFlag = false;
            string flaginString = string.Empty;
            try
            {
                flaginString = ConfigurationManager.AppSettings[confValue];
            }

            catch (ConfigurationErrorsException configErrEx)
            {
                throw configErrEx;
            }

            catch (Exception e)
            {
                throw e;
            }
            if (flaginString != null)
            {
                if (flaginString.ToLower().Equals("true"))
                {
                    returnFlag = true;
                }
            }
            return returnFlag;
        }
        public static HelperReturn isTruthyValueHelperObj(object obj)
        {
            string val = isTruthyValue(obj);
            HelperReturn h = new HelperReturn();
            if (!string.IsNullOrEmpty(val))
            {
                h.HasValue = true;
                h.Value = val;
            }
            return h;

        }

        public static async Task<Dictionary<string, dynamic>> GetRequestAsync(string url, bool Addheader = false)
        {   
            HttpClient client = new HttpClient();
            Task<HttpResponseMessage> response = null;
            Dictionary<string, dynamic> output = new Dictionary<string, dynamic>();
            try
            {
                
                client.BaseAddress = new Uri(url);
                client.Timeout = TimeSpan.FromMinutes(60);
                if (Addheader)
                {
                    client.DefaultRequestHeaders.Add("AccessCode", "Findex");
                }
                response = client.GetAsync(url);                
                if (response != null)
                {
                    if (response.Result != null && response.Result.ReasonPhrase != null)
                    {
                        output.Add("Status", response.Result.IsSuccessStatusCode);
                        output.Add("Message", response.Result.ToString());
                        string s = response.Result.Content.ReadAsStringAsync().Result;

                        output.Add("Output", s == "null" ? null : s);
                    }

                }
                await response;
                return output;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                output.Add("Status", response.IsFaulted);
                output.Add("Message", e.ToString());
                return output;
            }
            finally
            {
                client.Dispose();
            }
        }
        public static void GetRequestNTLMAsync(string url)
        {
            WebRequest req = WebRequest.Create(url);
            req.AuthenticationLevel = System.Net.Security.AuthenticationLevel.MutualAuthRequested;
            req.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
            WebResponse resp = req.GetResponse();
            StreamReader reader = new StreamReader(resp.GetResponseStream());
            var token = reader.ReadToEnd().Trim();
        }
        public async static Task<Dictionary<string, dynamic>> NTLMGet(string URL)
        {

            HttpClient client = new HttpClient();
            Task<HttpResponseMessage> response = null;
            try
            {
                // To Do
                client.BaseAddress = new Uri(URL);
                client.Timeout = TimeSpan.FromMinutes(5);
                response = client.GetAsync(URL);
                Dictionary<string, dynamic> output = new Dictionary<string, dynamic>();
                output.Add("Status", response.Result.IsSuccessStatusCode);
                output.Add("Message", Helper.isTruthyValue(response.Result));
                output.Add("StatusCode", Convert.ToInt32(response.Result.StatusCode));
                if (response.Result.IsSuccessStatusCode)
                {
                    output["Output"] = response.Result.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    output["Message"] = Helper.isTruthyValue(response.Result); //response.Result.Content.ReadAsStringAsync().Result;

                }
                await response;
                return output;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                client.Dispose();
            }
        }
        
        public static HelperReturn isTruthyValueIntObj(object obj)
        {
            HelperReturn h = new HelperReturn();
            int val = isTruthyValueInt(obj);
            if (val != 0)
            {
                h.HasValue = true;
                h.Value = val;
            }
            return h;
        }
        public static HelperReturn isTruthyValueObj(object obj)
        {
            HelperReturn h = new HelperReturn();
            string val = isTruthyValue(obj);
            if (val != string.Empty)
            {
                h.HasValue = true;
                h.Value = val;
            }
            return h;
        }

        public static int isTruthyValueInt(object obj)
        {
            int returnVal = 0;
            if (obj != null && !string.IsNullOrEmpty(obj.ToString()))
            {
                string text = Convert.ToString(obj);
                int.TryParse(removeInvalidXmlChars(text.Trim()), out returnVal);
            }
            return returnVal;
        }
        public static bool isTruthyValueBool(object obj)
        {
            bool returnFlag = false;

            try
            {
                if (obj != null && !string.IsNullOrEmpty(obj.ToString()))
                {
                    string text = Convert.ToString(obj);
                    text = removeInvalidXmlChars(text);
                    if (text != null)
                    {
                        if (text.ToLower().Equals("true") || text.Equals("1"))
                        {
                            returnFlag = true;
                        }
                    }

                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return returnFlag;
        }
        public static DataRow GetFirtDataRow(DataTable dt, int RowID = 0)
        {
            DataRow dr = null;
            if (dt != null && dt.Rows.Count > RowID && dt.Rows[RowID] != null)
            {
                dr = dt.Rows[RowID];
            }
            return dr;
        }

       
        public static DataTable GetDataTable(int curFileID, DataTable FileSearchInfoTable, string collumname)
        {
            bool takeflag = false;
            DataTable retDataTable = FileSearchInfoTable.Clone();
            foreach (DataRow row in FileSearchInfoTable.Rows)
            {
                if (curFileID == isTruthyValueInt(row[collumname]))
                {
                    retDataTable.ImportRow(row);
                    takeflag = true;
                }
                else if (takeflag)
                {
                    takeflag = false;
                    break;
                }
            }
            return retDataTable;
        }

        public static string CallURl(string URL)
        {
            var output = GetRequestAsync(URL);
            string status = Convert.ToString(output.Result["Status"]);
            if (status.Equals("false"))
            {
                return Convert.ToString(output.Result["Message"]);
            }
            else return status;

        }

    }

    enum DataTypeFAST
    {
        Default,
        Application,
        TypeCdId,
        AllTypeCdId
    }
    enum PhoneNumberFormat
    {
        [Description("###-#######")]
        Format1 = 1,
        [Description("##########")]
        Format2 = 2,
        [Description("###-###-####")]
        Format3 = 3,
        [Description("(###)###-####")]
        Format4 = 4
    }
    enum SearchParam
    {
        EntityType,
        City,
        State,
        Status,
        Region,
        ePeTFilter
    }
    public enum EPETFilter
    {
        Select,
        EP,
        ET,
        All,
        None
    }
    public enum DataTypeObj
    {
        DataSet,
        DataTable,
        SpecialDataSet,
        DataSetRawQuery,
        None
    }
    public class HelperReturn
    {
        public HelperReturn()
        {
            HasValue = false;

        }

        public bool HasValue { get; set; }
        public dynamic Value { get; set; }
    }

}


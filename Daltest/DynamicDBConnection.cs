using System;
//using DALLib;
using FAF.BT.DataAccess;
using System.Threading;

namespace Daltest
{
	/// <summary>
	/// Summary description for DynamicDBConnection.
	/// </summary>
	public class DynamicDBConnection : IDisposable
	{
		//private IDALTLS m_DALTls = null;
		public DynamicDBConnection(string strNewDBConnKey)
		{
			//For legacy BOs
			//m_DALTls = new DALTLSClass();
			//m_DALTls.ChangeConnection(strNewDBConnKey);
			
			//Set the TLS data for the NativeBOs
            // US #574851
            if (string.IsNullOrWhiteSpace(strNewDBConnKey)) return;
			Thread.SetData(Thread.GetNamedDataSlot(DAL.DALConStringDataSlotName),strNewDBConnKey);
		}
		#region IDisposable Members

		public void Dispose()
		{
			//m_DALTls.RemoveConnection();
			//System.Runtime.InteropServices.Marshal.ReleaseComObject(m_DALTls);
			Thread.SetData(Thread.GetNamedDataSlot(DAL.DALConStringDataSlotName),string.Empty);
		}

		#endregion
	}
    /*
	public class OverrideLegacyDBConnection : IDisposable 
	{
		//private IDALTLS m_DALTls = null;
		
		public OverrideLegacyDBConnection(string strRegConnKey)
		{
			//For legacy BOs
			m_DALTls = new DALTLSClass();
			m_DALTls.OverrideConnection(strRegConnKey);
		}
		#region IDisposable Members
		public void Dispose()
		{
			m_DALTls.RemoveOverrideConnection();
			System.Runtime.InteropServices.Marshal.ReleaseComObject(m_DALTls);
		}
		#endregion
	}*/
    public class OverrideNativeDBConnection : IDisposable
    {
        private object m_previousSetting = null;

        public OverrideNativeDBConnection(string strNewDBConnKey)
        {
            m_previousSetting = Thread.GetData(Thread.GetNamedDataSlot(DAL.DALConStringDataSlotName));
            Thread.SetData(Thread.GetNamedDataSlot(DAL.DALConStringDataSlotName), strNewDBConnKey);
        }
        #region IDisposable Members

        public void Dispose()
        {
            Thread.SetData(Thread.GetNamedDataSlot(DAL.DALConStringDataSlotName), m_previousSetting);
        }

        #endregion
    }
}

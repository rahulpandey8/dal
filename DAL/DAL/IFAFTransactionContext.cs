﻿using System;
using System.Data.SqlClient;

//using FAF.FrameworkBase;

namespace FAF.BT.DataAccess
{
    public interface IFAFTransactionContext
    {
        SqlTransaction SqlTrans { get; set; }
    }
}

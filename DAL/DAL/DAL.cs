using System;
using System.Data;
using System.Configuration;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
//using FAF.FrameworkBase;
using System.Threading;
namespace FAF.BT.DataAccess
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class DAL
	{
		private static string m_connectionString;
		public const string DALConStringDataSlotName = "__CSN";
		public const string DALConnectionObjectDataSlotName = "__CONOBJ";

		internal static string ConnectionString
		{
			get
			{
				object conStrSettingName = Thread.GetData(Thread.GetNamedDataSlot(DALConStringDataSlotName));
				if(conStrSettingName != null && (conStrSettingName.GetType() == typeof(System.String)) && conStrSettingName.ToString().Length > 0)
					return ConfigurationManager.AppSettings[conStrSettingName.ToString()];
				else
					return m_connectionString;
			}
		}
		public static FAFSqlConnWrapper CurrentConnection
		{
			set
			{
				Thread.SetData(Thread.GetNamedDataSlot(DALConnectionObjectDataSlotName),value);
			}
			get
			{
				return Thread.GetData(Thread.GetNamedDataSlot(DALConnectionObjectDataSlotName)) as FAFSqlConnWrapper;
			}
		}
		public static object ConnectionStringSettingForCurrentThread
		{
			set
			{
				Thread.SetData(Thread.GetNamedDataSlot(DALConStringDataSlotName),value);
			}
			get
			{
				return Thread.GetData(Thread.GetNamedDataSlot(DALConStringDataSlotName));
			}
		}

		static DAL()
		{
			m_connectionString = ConfigurationManager.AppSettings["ConnectionString"];	
		}

		public static SqlParameter[] GetSqlParameters(string storedProc)
		{
			SqlParameter[] sqlParams = SqlHelperParameterCache.GetCachedParameterSet(ConnectionString, storedProc);
			if (sqlParams == null)
				sqlParams = SqlHelperParameterCache.GetSpParameterSet(ConnectionString, storedProc);
			return sqlParams;
		}

		public static SqlParameter[] GetSqlParametersWithReturn(string storedProc) {
			return(SqlHelperParameterCache.GetSpParameterSet(ConnectionString, storedProc, true));
		}

// JG: Modified the following function to return SqlParameter[] instead of object[]
// JG: Reasons:
// JG: a) GetSqlParameters() is returning a cloned array of sql parameters, which is being used
//        only to determine the size of the param object[] (coule be used more efficiently). Returning
//        the SqlParameters[] reduces the call to the clone param functionality for the second time.
//     b) To support DAL.Executes with in the same transaction context of BO Save. Call to derive
//        parameters on SqlCommandBuilder fails if there is a pending transaction in the connection used.
//        Passing the SqlParameter[] to the SqlHelper bypasses this call to derive params.
		private static SqlParameter[] ValidateParams(string storedProc, object[] parms)
		{
			SqlParameter[] sqlParams = GetSqlParameters(storedProc);

			if (parms != null && parms.Length > 0)
			{
				if (parms.Length > sqlParams.Length)
					throw new ArgumentException("Parameter count does not match Parameter Value count.");

                for (int i = 0; i < sqlParams.Length && i < parms.Length ; i++)
					sqlParams[i].Value = parms[i];
			}

			return sqlParams;
		}

		public static SqlConnection CreateOpenConnection()
		{
			SqlConnection connection = new SqlConnection(ConnectionString);
			connection.Open();
			return connection;
		}

		public static SqlTransaction CreateTransaction(SqlConnection connection)
		{
			return connection.BeginTransaction(IsolationLevel.ReadUncommitted);
		}

		// JG: Modified code to handle SqlParameter[] returned by ValidateParams() instead of object[].
		// JG: Avoids an additional call to the clone parameters functionality on SqlHelper
		public static DataSet ExecuteStoredProc(string storedProc, params object[] parms)
		{
			try
			{
				SqlParameter[] spParams = ValidateParams(storedProc, parms);
				TimeSpan startTime = new TimeSpan(DateTime.Now.Ticks);
				DataSet dsRet = null;
				if(DAL.CurrentConnection != null && DAL.CurrentConnection.ConnectionString.Equals(ConnectionString))
					dsRet = SqlHelper.ExecuteDataset(DAL.CurrentConnection.Connection, CommandType.StoredProcedure, storedProc, spParams);
				else
					dsRet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, storedProc, spParams);
				
				return dsRet;
			}
			catch(Exception ex)
			{
				System.Text.StringBuilder stringBuf = new System.Text.StringBuilder(100);
				stringBuf.Append("ExecuteStoredProc Failed. Stroed Proc - ");
				stringBuf.Append(StoredProcDebugInfo(storedProc,parms));
				throw;
			}
		}
		
		// JG: Modified code to handle SqlParameter[] returned by ValidateParams() instead of object[].
		// JG: a) Avoids an additional call to clone parameters on the sqlhelper.
		// JG: b) SqlCommandBuilder.DeriveParameters fail to execute on a connection with a pending transaction.
		// JG:    This call would fail for SP executes. (notes: worked fine earlier if the SP is not accepting any params)
		public static DataSet ExecuteStoredProc(SqlTransaction sqlTrans,string storedProc, params object[] parms)
		{
			try
			{
				SqlParameter[] spParams = ValidateParams(storedProc, parms);
				TimeSpan startTime = new TimeSpan(DateTime.Now.Ticks);
				DataSet dsRet = null;
				
					dsRet = SqlHelper.ExecuteDataset(sqlTrans, CommandType.StoredProcedure, storedProc, spParams);
				
				return dsRet;
			}
			catch(Exception ex)
			{
				System.Text.StringBuilder stringBuf = new System.Text.StringBuilder(100);
				stringBuf.Append("ExecuteStoredProc Failed. Stroed Proc - ");
				stringBuf.Append(StoredProcDebugInfo(storedProc,parms));
				throw;
			}
		}

        public static DataSet ExecuteStoredProc(IFAFTransactionContext transContext, string storedProc, params object[] parms)
        {
            return ExecuteStoredProc(transContext.SqlTrans, storedProc, parms);
        }

		private static string StoredProcDebugInfo(string spName, object[] parms)
		{
			System.Text.StringBuilder stringBuf = new System.Text.StringBuilder(100);
			stringBuf.Append(spName);
			if(parms != null)
			{
				foreach(object parm in parms)
				{
					if(parm != null)
						stringBuf.Append(parm.ToString());
					else
						stringBuf.Append("NULL");
					stringBuf.Append(", ");
				}
			}
			return stringBuf.ToString();
		}

		public static DataSet ExecuteBatchSave(SqlTransaction sqlTrans, string sqlBatch)
		{
			TimeSpan startTime = new TimeSpan(DateTime.Now.Ticks);
			DataSet dsRet = SqlHelper.ExecuteDataset(sqlTrans, CommandType.Text, sqlBatch);
			return dsRet;
		}

        public static DataSet ExecuteBatchSave(IFAFTransactionContext transContext, string sqlBatch)
        {
            return ExecuteBatchSave(transContext.SqlTrans, sqlBatch);
        }

		public static object[] Execute(string spName, params object[] parms)
		{
			try
			{
				SqlParameter[] sqlParams = GetSqlParameters(spName);
			
				for (int i=0; i < parms.Length; i++)
					sqlParams[i].Value = parms[i];

				if(DAL.CurrentConnection != null && DAL.CurrentConnection.ConnectionString.Equals(ConnectionString))
					SqlHelper.ExecuteNonQuery(DAL.CurrentConnection.Connection, CommandType.StoredProcedure, spName, sqlParams);
				else
					SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.StoredProcedure, spName, sqlParams);
			
				object[] results = new object[sqlParams.Length];

				for (int i=0; i < sqlParams.Length; i++)
					results[i] = sqlParams[i].Value;
				
				return results;
			}
			catch(Exception ex)
			{
				System.Text.StringBuilder stringBuf = new System.Text.StringBuilder(100);
				stringBuf.Append("Execute Failed. Stroed Proc - ");
				stringBuf.Append(spName);
				foreach(object parm in parms)
				{
					stringBuf.Append(parm.ToString());
					stringBuf.Append(", ");
				}
				throw;
			}
			
		}
        public static object[] Execute(int TimeOut, string spName, params object[] parms)
        {
            try
            {
                SqlParameter[] sqlParams = GetSqlParameters(spName);

                for (int i = 0; i < parms.Length; i++)
                    sqlParams[i].Value = parms[i];

                if (DAL.CurrentConnection != null && DAL.CurrentConnection.ConnectionString.Equals(ConnectionString))
                    SqlHelper.ExecuteNonQuery(DAL.CurrentConnection.Connection, TimeOut, CommandType.StoredProcedure, spName, sqlParams);
                else
                    SqlHelper.ExecuteNonQuery(ConnectionString, TimeOut, CommandType.StoredProcedure, spName, sqlParams);

                object[] results = new object[sqlParams.Length];
                for (int i = 0; i < sqlParams.Length; i++)
                    results[i] = sqlParams[i].Value;

                return results;
            }
            catch (Exception ex)
            {
                System.Text.StringBuilder stringBuf = new System.Text.StringBuilder(100);
                stringBuf.Append("Execute Failed. Stroed Proc - ");
                stringBuf.Append(spName);
                foreach (object parm in parms)
                {
                    stringBuf.Append(parm.ToString());
                    stringBuf.Append(", ");
                }
                throw;
            }

        }

		public static object[] Execute(SqlTransaction sqlTrans, string spName, params object[] parms)
		{
			try
			{
				SqlParameter[] sqlParams = GetSqlParameters(spName);
			
				for (int i=0; i < parms.Length; i++)
					sqlParams[i].Value = parms[i];

				SqlHelper.ExecuteNonQuery(sqlTrans, CommandType.StoredProcedure, spName, sqlParams);
							
				object[] results = new object[sqlParams.Length];

				for (int i=0; i < sqlParams.Length; i++)
					results[i] = sqlParams[i].Value;
				
				return results;
			}
			catch(Exception ex)
			{
				System.Text.StringBuilder stringBuf = new System.Text.StringBuilder(100);
				stringBuf.Append("Execute Failed. Stroed Proc - ");
				stringBuf.Append(spName);
				foreach(object parm in parms)
				{
					stringBuf.Append(parm.ToString());
					stringBuf.Append(", ");
				}
				throw;
			}
		}

        public static object[] Execute(IFAFTransactionContext transContext, string spName, params object[] parms)
        {
            return Execute(transContext.SqlTrans, spName, parms);
        }

		public static void ExecuteBatch(string sqlBatchText)
		{
			try
			{
				if (DAL.CurrentConnection != null && DAL.CurrentConnection.ConnectionString.Equals(ConnectionString))
					SqlHelper.ExecuteNonQuery(DAL.CurrentConnection.Connection, CommandType.Text, sqlBatchText);
				else
					SqlHelper.ExecuteNonQuery(ConnectionString, CommandType.Text, sqlBatchText);
			}
			catch(Exception ex)
			{
				throw ex;
			}
		}

		public static DataSet ExecuteStoredProcWithOutput(string storedProc, out SqlParameter[] outParms, params object[] inParms) 
		{
			try {
				SqlParameter[] sqlParms = GetSqlParametersWithReturn(storedProc);
				if (inParms != null) {
					for (int i=0; i<inParms.Length; ++i)
						sqlParms[i+1].Value = inParms[i];
				}
				TimeSpan startTime = new TimeSpan(DateTime.Now.Ticks);
				DataSet dsRet = null;
				if(DAL.CurrentConnection != null && DAL.CurrentConnection.ConnectionString.Equals(ConnectionString))
					dsRet = SqlHelper.ExecuteDataset(DAL.CurrentConnection.Connection, CommandType.StoredProcedure, storedProc, sqlParms);
				else
					dsRet = SqlHelper.ExecuteDataset(ConnectionString, CommandType.StoredProcedure, storedProc, sqlParms);
				outParms = sqlParms;
				return dsRet;
			} catch(Exception ex) {
				System.Text.StringBuilder stringBuf = new System.Text.StringBuilder(100);
				stringBuf.Append("ExecuteStoredProc Failed. Stroed Proc - ");
				stringBuf.Append(StoredProcDebugInfo(storedProc,inParms));
				throw;
			}
		}

        public static DataSet ExecuteStoredProcWithOutput(IFAFTransactionContext  transContext, string storedProc, out SqlParameter[] outParms, params object[] inParms)
        {
            return ExecuteStoredProcWithOutput(transContext.SqlTrans, storedProc, out outParms, inParms);
        }
        public static DataSet ExecuteStoredProcWithOutput(IFAFTransactionContext transContext, string storedProc, out object[] outParms, params object[] inParms)
        {
            return ExecuteStoredProcWithOutput(transContext.SqlTrans, storedProc, out outParms, inParms);
        }

        public static DataSet ExecuteStoredProcWithOutput(SqlTransaction sqlTrans, string storedProc, out SqlParameter[] outParms, params object[] inParms)
        {
            try
            {
                SqlParameter[] sqlParms = GetSqlParametersWithReturn(storedProc);
                if (inParms != null)
                {
                    for (int i = 0; i < inParms.Length; ++i)
                        sqlParms[i + 1].Value = inParms[i];
                }
                TimeSpan startTime = new TimeSpan(DateTime.Now.Ticks);
                DataSet dsRet = null;
                dsRet = SqlHelper.ExecuteDataset(sqlTrans, CommandType.StoredProcedure, storedProc, sqlParms);
              
                outParms = sqlParms;
                return dsRet;
            }
            catch (Exception ex)
            {
                System.Text.StringBuilder stringBuf = new System.Text.StringBuilder(100);
                stringBuf.Append("ExecuteStoredProcWithOutput Failed. Stroed Proc - ");
                stringBuf.Append(StoredProcDebugInfo(storedProc, inParms));
                
                throw;
            }
        }

        public static DataSet ExecuteStoredProcWithOutput(SqlTransaction sqlTrans, string storedProc, out object[] outParms, params object[] inParms)
        {
            try
            {
                SqlParameter[] sqlParms = GetSqlParametersWithReturn(storedProc);
                if (inParms != null)
                {
                    for (int i = 0; i < inParms.Length; ++i)
                        sqlParms[i + 1].Value = inParms[i];
                }
                TimeSpan startTime = new TimeSpan(DateTime.Now.Ticks);
                DataSet dsRet = null;
                dsRet = SqlHelper.ExecuteDataset(sqlTrans, CommandType.StoredProcedure, storedProc, sqlParms);
               
                outParms = new object[sqlParms.Length];
                for (int i = 0; i < sqlParms.Length; i++)
                    outParms[i] = sqlParms[i].SqlValue;
                return dsRet;
            }
            catch (Exception ex)
            {
                System.Text.StringBuilder stringBuf = new System.Text.StringBuilder(100);
                stringBuf.Append("ExecuteStoredProcWithOutput Failed. Stroed Proc - ");
                stringBuf.Append(StoredProcDebugInfo(storedProc, inParms));
                throw;
            }
        }

		public static DataSet ExecuteStoredProcWithOutput(string storedProc, out object[] outParms, params object[] inParms) {
			SqlParameter[] output;
			DataSet tmpDS = ExecuteStoredProcWithOutput(storedProc, out output, inParms);
			outParms = new object[output.Length];
			for (int i=0; i<output.Length; ++i) {
				outParms[i] = output[i].Value;
			}
			return(tmpDS);
		}
	}
}

using System;
//using FAF.FrameworkBase;
using System.Data.SqlClient;
using System.Data;
using Microsoft.ApplicationBlocks.Data;

namespace FAF.BT.DataAccess
{
	/// <summary>
	/// Summary description for SqlConnWrapper.
	/// </summary>
	public class FAFSqlConnWrapper: IDisposable
	{
		private SqlConnection m_connection = null;
		private string m_connectionString = null;
		private FAFSqlConnWrapper m_prevConnection = null;
		private int m_nestingLevel = 0;

		public FAFSqlConnWrapper()
		{
			#region "Comments"
			//JG - 15-May-08
			//1.	moved initialization of m_connectionString & m_connection to constructor.
			//		this would be more deterministic.
			//		Earlier, these were initialized based on the first DB hit, not based on 
			//		the connection string setting at the time of FAFSqlConnWrapper creation.
			//		For eg:
			//			using(new FAFSqlConnWrapper())
			//			{
			//				DAL.ConnectionStringSettingForCurrentThread = "BPM_Connection";
			//				// Load BO or Execute SP on BPM DB
			//				DAL.ConnectionStringSettingForCurrentThread = "";
			//				// Load BO or execute SP on FAST DB
			//				// Load BO or execute SP on FAST DB
			//				// Load BO or execute SP on FAST DB
			//				// Load BO or execute SP on FAST DB
			//			}
			//		For this scenario, instead of caching FAST connection, it caches BPM connection,
			//		which is confusing.
			//
			//2.	to support nested cached connection usage, added else condition to if(DAL.CurrentConnection == null)
			//		using(new FAFSqlConnWrapper())
			//		{
			//			// do some DB operations
			//			DAL.ConnectionStringSettingForCurrentThread = "SomethingElse";
			//			using(new FAFSqlConnWrapper())
			//			{
			//				// do some DB operations
			//			}
			//		}
			#endregion
			if(DAL.CurrentConnection == null)
			{
				DAL.CurrentConnection = this;
				m_connectionString = DAL.ConnectionString;
				m_connection = new SqlConnection(m_connectionString);
				m_nestingLevel += 1;
			}
			else
			{
				if (DAL.CurrentConnection.ConnectionString != DAL.ConnectionString)
				{
					m_prevConnection = DAL.CurrentConnection;
					m_nestingLevel = m_prevConnection.NestingLevel + 1;
					DAL.CurrentConnection = this;
					m_connectionString = DAL.ConnectionString;
					m_connection = new SqlConnection(m_connectionString);
				}
			}
		}
		public String ConnectionString
		{
			get
			{
				//JG - 15-May-08
				//do not create/open a connection while accessing ConnectionString property
				//if(m_connectionString == null || m_connection.State == ConnectionState.Closed) // Initial
				//{
				//	CreateConnection();
				//}
				return m_connectionString;
			}
		}
		private void CreateConnection()
		{
			// JG - 15-May-08 - removed code initializing connection string and connection object
			m_connection.Open();
            SqlHelper.ExecuteNonQuery(m_connection, CommandType.Text, "SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED");
		}
		public SqlConnection Connection
		{
			get
			{
				// JG: 15-May-08
				// if(m_connection == null || m_connection.State == ConnectionState.Closed)
				if(m_connection != null && m_connection.State == ConnectionState.Closed)
				{
					CreateConnection();
				}
				return m_connection;
			}
		}
		public int NestingLevel
		{
			get
			{
				return m_nestingLevel;
			}
		}
		private string GetDBNameFromConnStr(string connStr)
		{
            //Get just the DB names for log files. Avoids printing the entire connection string with the password,
            //might not be good for production.
			string dbName = String.Empty;
			int i,j;
			i = connStr.IndexOf("Initial Catalog=");
			if (i >= 0)
			{
				i = i + "Initial Catalog=".Length;
				j = connStr.IndexOf(";", i);
				if (j < 0) j = connStr.Length;
				dbName = connStr.Substring(i, j - i);
			}
			return dbName;
		}

		#region IDisposable Members

        //Note: Finalize method is not implemented.
        //      Since it deals with DB connections, developer has to make sure to release by calling dispose.
		public void Dispose()
		{
			if(m_connection != null)
			{
				if (m_connection.State != ConnectionState.Closed)
				{
					m_connection.Close();
				}
				m_connection.Dispose();
				//JG - 15-May-08 - to support nested TLSConnection, revert back to the previous connection in use
				DAL.CurrentConnection = m_prevConnection;
			}
		}
		#endregion
	}
}
